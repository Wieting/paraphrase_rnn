function [pairs] = getPairs(sample,words)

mat = zeros(25,2*length(sample));

for i=1:1:length(sample)
    mat(:,2*i-1) = sample{i}{1}.nodeFeaturesforward(:,end);
    mat(:,2*i) = sample{i}{2}.nodeFeaturesforward(:,end);
    words(sample{i}{1}.nums);
    words(sample{i}{2}.nums);
end


%pick closest tree as negative.
pairs = {};
for i=1:1:length(sample)
    s = sample{i};
    x1 = repmat(s{1}.nodeFeaturesforward(:,end),[1,length(sample)*2]);
    dp1 = sum((x1.*mat));
    
    x2 = repmat(s{2}.nodeFeaturesforward(:,end),[1,length(sample)*2]);
    dp2 = sum((x2.*mat));
    gg = dp1(2*i);
    
    mintree1 = {};
    mintree2 = {};
    mintree1score = -5;
    mintree2score = -5;
    for j=1:1:length(dp1)
        idxj = round(j/2);
        if(idxj==i)
            continue;
        end
        if(dp1(j) > mintree1score)
            mintree1score = dp1(j);
            if(mod(j,2)==1)
                mintree1 = sample{round(j/2)}{1};
            else
                mintree1 = sample{j/2}{2};
            end
        end
        
        if(dp2(j) > mintree2score)
            mintree2score = dp2(j);
            if(mod(j,2)==1)
                mintree2 = sample{round(j/2)}{1};
            else
                mintree2 = sample{j/2}{2};
            end
        end
    end
    %[words(s{1}.nums) words(mintree1.nums)];
    pairs{end+1} = {mintree1 mintree2};
end

end