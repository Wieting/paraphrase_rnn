clear;
we = '../data/turian25';
initv = '../data/theta_init_25';
hiddenSize = 25;
o1='../data/turian25_s_data2';
o2='';
makeDataSmall(we,initv,hiddenSize,o1,o2);

%clear;
%we = '../data/skipbll25';
%initv = '../data/theta_init_25';
%hiddenSize = 25;
%o1='../data/skipbll25_s_data';
%o2='../data/skipbll25_test_data';
%makeDataSmall(we,initv,hiddenSize,o1,o2);

clear;
we = '../data/skipwiki25';
initv = '../data/theta_init_25';
hiddenSize = 25;
o1='../data/skipwiki25_s_data2';
o2='';
makeDataSmall(we,initv,hiddenSize,o1,o2);

%clear;
%we = '../data/skipbll50';
%initv = '../data/theta_init_50';
%hiddenSize = 50;
%o1='../data/skipbll50_s_data';
%o2='../data/skipbll50_test_data';
%makeDataSmall(we,initv,hiddenSize,o1,o2);

clear;
we = 'skipwiki50';
initv = 'theta_init_50';
hiddenSize = 50;
o1='skipwiki50.adjnoun.mat';
makeDataSmall(we,initv,hiddenSize,o1);

clear;
addpath('../basic');
we = '../autoencoder/vars.normalized.100.mat';
initv = '../autoencoder/params.mat';
hiddenSize = 100;
o1 = 'socher100.adjnoun.mat';
makeDataSmall(we,initv,hiddenSize,o1);

