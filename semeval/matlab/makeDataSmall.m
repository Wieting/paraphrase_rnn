function [] = makeDataSmall(we,initv,hiddenSize,output1)

load(we);
load(initv);
if(hiddenSize == 100)
    We_orig = We2;
end

W1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
W2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
bw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);

inputFile = '../data/adj_noun_phrase_1_parsed.txt';
parseTreesp1 = getParseTrees(inputFile,words);
inputFile = '../data/adj_noun_phrase_2_parsed.txt';
parseTreesp2 = getParseTrees(inputFile,words);
labels=load('../data/adj_noun_phrase_labels');
%labels = load('../../jack_words2/words/test_labels.txt');

[Treesp1, ~] = getTrees2(parseTreesp1, W1, W2, bw1, We_orig, hiddenSize);
[Treesp2, ~] = getTrees2(parseTreesp2, W1, W2, bw1, We_orig, hiddenSize);

data = {};
for i=1:1:length(Treesp1)
    data{end+1} = {Treesp1{i}; Treesp2{i}; labels(i)};
end
data = data(randperm(numel(data)));
train_data={};
valid_data={};
test_data={};
for i=1:1:round(.8*length(Treesp1))
    train_data{end+1} = data{i};
end

for i=round(.8*length(Treesp1))+1:1:round(.9*length(Treesp1))
    valid_data{end+1} = data{i};
end

for i=round(.9*length(Treesp1))+1:1:length(Treesp1)
    test_data{end+1} = data{i};
end

save(output1,'train_data','valid_data','test_data');

end
