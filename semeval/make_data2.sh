alias matlab=/Applications/MATLAB_R2014a.app/bin/matlab

train=SICK_train.txt
test=SICK_test_annotated.txt
dev=SICK_trial.txt

#cd python
#python prep_semeval.py ../$train
#python prep_semeval.py ../$test
#python prep_semeval.py ../$dev

#cd ..

#cd stanford-parser-2011-09-14/
#sh lexparser.sh ../$train-1 > ../$train-1-parsed
#sh lexparser.sh ../$train-2 > ../$train-2-parsed
#sh lexparser.sh ../$test-1 > ../$test-1-parsed
#sh lexparser.sh ../$test-2 > ../$test-2-parsed
#sh lexparser.sh ../$dev-1 > ../$dev-1-parsed
#sh lexparser.sh ../$dev-2 > ../$dev-2-parsed

#cd ..

cd matlab
matlab -nodisplay -nodesktop -nojvm -nosplash -r "make_mat('../../single_W/code/test/data/skipwiki50.mat','../../single_W/code/test/data/theta_init_50.mat',50,'../$train-1-parsed','../$train-2-parsed','../$train-scores','../$train.50.mat');quit"

matlab -nodisplay -nodesktop -nojvm -nosplash -r "make_mat('../../single_W/code/test/data/skipwiki50.mat','../../single_W/code/test/data/theta_init_50.mat',50,'../$test-1-parsed','../$test-2-parsed','../$test-scores','../$test.50.mat');quit"

matlab -nodisplay -nodesktop -nojvm -nosplash -r "make_mat('../../single_W/code/test/data/skipwiki50.mat','../../single_W/code/test/data/theta_init_50.mat',50,'../$dev-1-parsed','../$dev-2-parsed','../$dev-scores','../$dev.50.mat');quit"


stty sane
cd ..
#rm $fname*
