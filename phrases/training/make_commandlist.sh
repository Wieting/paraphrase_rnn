
python make_tti_commands_single.py ../../make_data/phrases_filtered/filtered_training_100k.txt.mat filtered_100k 0.05 > filtered_test.txt
python make_tti_commands_single.py ../../make_data/phrases_unfiltered/nonfiltered_training_100k.txt.mat unfiltered_100k 0.05 > unfiltered_test.txt

cat unfiltered_test.txt filtered_test.txt > all_test.txt

python make_tti_commands_single.py ../../make_data/phrases_filtered/filtered_training_100k.txt.mat filtered_100k 1.0 > filtered.txt
python make_tti_commands_single.py ../../make_data/phrases_unfiltered/nonfiltered_training_100k.txt.mat unfiltered_100k 1.0 > unfiltered.txt

cat unfiltered.txt filtered.txt > all.txt