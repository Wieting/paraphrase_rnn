import sys
import os

outfile = "../../params/skipwiki25_phrases_100k_single_"
data = sys.argv[1]
fname = sys.argv[2]
frac = sys.argv[3]
#../../make_data/phrases_filtered/filtered_training_100k.txt.mat
#filtered_100k
#1.0

lr1=0.05
lr2=0.50

lambda1 = [0, 0.1, 0.01, 0.001]
lambda2 = [0.01, 0.001, 0.0001, 0.00001, 0.000001]

for i in lambda1:
    for j in lambda2:
        cmd = str(i)+", "+str(j)+", "+frac+", '"+outfile+str(i)+"_"+str(j)+"_"+fname+"', '"+data+"'"
        cmd = "/opt/matlab-r2013a/bin/matlab -nodisplay -nodesktop -nojvm -nosplash -r \"train_single("+cmd+");quit\""
        print cmd