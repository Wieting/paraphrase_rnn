addpath('../basic');
load('../data/skipwiki25.adjnoun.mat');
load('../data/skipwiki25.mat');
clear train_data;
hiddenSize = 25;

sample = {};
for i=1:1:length(valid_data)
    l = valid_data{i};
    label = l{3};
    if(label > 0.5)
        sample{end+1} = l;
    end
    if(length(sample) > 2000-1)
        break;
    end
end

load('../data/theta_init_25.mat');
W1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
W2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
bw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);

sample = feedForwardTrees(sample, theta, hiddenSize, We_orig);

[We_new, sample_new, wordsT] = limitWords(sample, We_orig, words);
pairs = getPairs(sample_new,wordsT);
[pairs, sample_new] = getPairsBatch(sample_new,wordsT,100);

thetafortraining = theta;
Wefortraining = reshape(We_new,numel(We_new),1);
Wefortraining_m = We_new;


%check numerical gradients
objectiveWordDerNew(thetafortraining, hiddenSize, sample_new, pairs, wordsT, We_new, Wefortraining_m, 0, 0, 1,1)
for i=1:1:30
    %sample_new = feedForwardTrees(sample_new, thetafortraining, hiddenSize, We_new);
    pairs = getPairs(sample_new,wordsT);
    %numgrad = computeNumericalGradient( @(x) objectiveWordDerNew(x, hiddenSize, sample_new,pairs, wordsT, We_new, We_new,0,0,1,0), thetafortraining);
    numgrad = computeGradNewObj(sample_new, pairs, thetafortraining, hiddenSize, wordsT, We_new, 0, 1);
    thetafortraining = thetafortraining - .1*numgrad;
    %numgrad = computeNumericalGradient( @(x) objectiveWordDerNew(thetafortraining, hiddenSize, sample_new,pairs, wordsT, We_new, x,0,0,1,0), Wefortraining);
    numgrad = computeGradNewObjWords(sample_new, pairs, thetafortraining, hiddenSize, wordsT, We_new, Wefortraining_m, 0, 1);
    %Wefortraining = Wefortraining - 1*numgrad;
    %Wefortraining_m = reshape(Wefortraining,hiddenSize,numel(Wefortraining)/hiddenSize);
    Wefortraining_m = Wefortraining_m - 1*numgrad;
    objectiveWordDerNew(thetafortraining, hiddenSize, sample_new, pairs, wordsT, We_new, Wefortraining_m, 0, 0, 1,1)
end

%test matrix derivative
thetafortraining = theta;
Wefortraining = reshape(We_new,numel(We_new),1);
Wefortraining_m = We_new;

tic
numgrad_theta = computeNumericalGradient( @(x) objectiveWordDerNew(x, hiddenSize, sample_new,pairs, wordsT, We_new, We_new,.5,.5,1,0), thetafortraining);
toc
tic
numgrad_test = computeGradNewObj(sample_new, pairs, thetafortraining, hiddenSize, wordsT, We_new, .5, 1);
toc
numgrad_test(1:5);
numgrad_theta(1:5);
scoreGradient(numgrad_theta, numgrad_test)

%test word derivatives


thetafortraining = theta;
Wefortraining = reshape(We_new,numel(We_new),1);
Wefortraining_m = We_new;

tic
numgrad_We = computeNumericalGradient( @(x) objectiveWordDerNew(thetafortraining, hiddenSize, sample_new,pairs, wordsT, We_new, x,.5,.5,1,0), Wefortraining);
toc
tic
numgrad_test = computeGradNewObjWords(sample_new, pairs, thetafortraining, hiddenSize, wordsT, We_new, We_new, .5, 1);
toc
numgrad_test = reshape(numgrad_test, [numel(numgrad_test) 1]);
numgrad_test(50:55);
numgrad_We(50:55);
scoreGradient(numgrad_We, numgrad_test)




%test for pairs
for i=1:1:length(pairs)
    v1 = pairs{i}{1}.nodeFeaturesforward(:,end);
    v2 = pairs{i}{2}.nodeFeaturesforward(:,end);
    
    g1 = sample{i}{1}.nodeFeaturesforward(:,end);
    g2 = sample{i}{2}.nodeFeaturesforward(:,end);
    [sum(g1.*g2) sum(g1.*v1) sum(g2.*v2) sum(g1.*v2) sum(g2.*v1)];
    [words(pairs{i}{1}.nums) words(pairs{i}{2}.nums)  words(sample{i}{1}.nums)  words(sample{i}{2}.nums)]
    [words(sample{i}{1}.nums)  words(sample{i}{2}.nums)];
end



hiddenSize = 25;
sample = {};
for i=1:1:length(valid_data)
    l = valid_data{i};
    label = l{3};
    if(label > 0.5)
        sample{end+1} = l;
    end
    if(length(sample) > 2000-1)
        break;
    end
end
load('../data/theta_init_25.mat');
W1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
W2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
bw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);
params.data = sample;
params.margin = 1;
params.batchsize = round(0.5*length(sample_new));
params.etat = 0.05;
params.etaw = .5;
params.lambda_t = .01;
params.lambda_w = .001;
params.epochs = 20;
outfile = 'test_data';
AGWords_newobj(theta, params, hiddenSize, words, We_orig, outfile);