function [data, nodeParams] = feedForwardTrees(data, map, nodeParams, theta, hiddenSize, We)

rW1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
rW2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
rbw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);

for ii=1:1:length(data)
    %fprintf('Computing cost for tree %d\n',ii);
    currTreep1 = data{ii}{1};
    currTreep2 = data{ii}{2};
    [treesp1, nodeParams] = forwardpassWordDer(nodeParams, currTreep1, map, theta, hiddenSize, We);
    [treesp2, nodeParams] = forwardpassWordDer(nodeParams, currTreep2, map, theta, hiddenSize, We);
    data{ii}{1} = treesp1;
    data{ii}{2} = treesp2;
end

end