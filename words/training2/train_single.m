function [] = train_single(l1,l2,frac,output,datafile,batchsize)
addpath(genpath('../code/'));
hiddenSize = 25;
params.etat=.05;
params.etaw=.5;
params.lambda_t=l1;
params.lambda_w=l2;
params.margin = 1;

load('../code/test/data/skipwiki25.mat');

fid = fopen(datafile,'r');
data = textscan(fid,'%s%s', 'delimiter','\t');
play_data = {};
for i=1:1:length(data{1})
play_data{end+1} = {data{1}{i} data{2}{i}};
end
global wordMap;
wordMap = containers.Map(words,1:length(words));
temp={};
for i=1:1:length(play_data)
temp{end+1}=[WordLookup(play_data{i}{1}) WordLookup(play_data{i}{2})];
end
play_data=temp;

p = randperm(length(play_data));
train_data=play_data(p);

%use adagrad
params.epochs = 2;
params.data= train_data(1:round(frac*length(train_data)));
fprintf('Training on %i data using %f and %f\n',length(params.data),l1,l2);

params.batchsize = batchsize;
fprintf('Training on %d instances.\n',length(params.data));

AGWords(params, hiddenSize, words, We_orig, output);
end