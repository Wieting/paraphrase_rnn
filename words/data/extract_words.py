def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

f = open('vocab.skip25w5-wiki.en','r')
lines = f.readlines()

vocab = []
for i in lines:
    i=i.strip()
    vocab.append(i)

vocab = set(vocab)

f = open('ppdb-1.0-xl-lexical','r')
lines = f.readlines()

lis=[]

for i in lines:
    i=i.split('|||')
    w1 = i[1].strip()
    w2 = i[2].strip()
    if(hasNumbers(w1) == False and hasNumbers(w2) == False and w1 in vocab and w2 in vocab):
        if(w1 < w2):
            lis.append(w1+"\t"+w2)
        else:
            lis.append(w2+"\t"+w1)

lis = list(set(lis))

for i in lis:
    print i

#print len(lis)
