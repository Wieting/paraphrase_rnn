rng(1);

inputFile = 'parsed3.txt';
load('jack_variables');
hiddenSize = 2;
lambda = 0.0001;

%initialize
r  = sqrt(6) / sqrt(hiddenSize+hiddenSize+1);   % we'll choose weights uniformly from the interval [-r, r]
W1 = rand(hiddenSize, hiddenSize) * 2 * r - r;
W2 = rand(hiddenSize, hiddenSize) * 2 * r - r;
bw1 = rand(hiddenSize, 1) * 2 * r - r;

V1 = rand(hiddenSize, hiddenSize) * 2 * r - r;
V2 = rand(hiddenSize, hiddenSize) * 2 * r - r;
bv1 = rand(hiddenSize, 1) * 2 * r - r;
bv2 = rand(hiddenSize, 1) * 2 * r - r;

theta = [W1(:); W2(:) ; bw1(:) ; V1(:) ; V2(:) ; bv1(:) ; bv2(:) ];

parseTrees = getParseTrees(inputFile,words);
Trees = getTrees3(parseTrees, We_orig, hiddenSize);

for i=1:1:length(Trees)
    Trees{i}.subtrees = getSubTrees(Trees{i},words,hiddenSize,We_orig);
end

trainset = Trees(1:1);

numgrad = computeNumericalGradient( @(x) computeCostAllSubtreesParallel(x, hiddenSize, trainset, words, We_orig), theta);
grad = computeGradAllSubtrees(trainset, theta, hiddenSize, words, We_orig);
scoreGradient(numgrad,grad)
