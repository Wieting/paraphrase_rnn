import sys
import os

outfile = "../../params/skipwiki25_bigrams_single_"
data = sys.argv[1]
fname = sys.argv[2]
frac = sys.argv[3]
#../../make_data/bigrams/adj-noun-base-xl.txt
#adj-noun-xl
#best for adj-noun: 0.01/0.001, 0/0.001, 0.01/0.0001, 0.1/0.0001, 0.001/0.001
#best for noun-noun: 0.01/0.001, 0/0.001, 0.01/0.01, 0.001/0.000001, 0.01/0.0001
#best for verb-noun: 0.1/0.00001, 0.1/0.000001, 0.1/0.0001, 0.001/0.001, 0/0.01

an1 = [0.01, 0, 0.01, 0.1, 0.001]
an2 = [0.001, 0.001, 0.0001, 0.0001, 0.001]
nn1 = [0.01, 0, 0.01, 0.001, 0.01]
nn2 = [0.001, 0.001, 0.01, 0.000001, 0.0001]
vn1 = [0.1, 0.1, 0.1, 0.001, 0]
vn2 = [0.00001, 0.000001, 0.0001, 0.001, 0.01]

lr1=0.05
lr2=0.50

lambda1 = []
lambda2 = []

if('adj-noun' in fname):
    lambda1=an1
    lambda2=an2
if('noun-noun' in fname):
    lambda1=nn1
    lambda2=nn2
if('verb-noun' in fname):
    lambda1=vn1
    lambda2=vn2

bs=[0.025,0.01,0.005,0.0025]

for j in range(len(bs)):
    for i in range(len(lambda1)):
        cmd = str(lambda1[i])+", "+str(lambda2[i])+", "+frac+", '"+outfile+str(lambda1[i])+"_"+str(lambda2[i])+"_"+str(bs[j])+"_"+fname+"', '"+data+"', "+str(bs[j])
        cmd = "/opt/matlab-r2013a/bin/matlab -nodisplay -nodesktop -nojvm -nosplash -r \"train_single("+cmd+");quit\""
        print cmd