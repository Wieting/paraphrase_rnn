function [] = train_single(l1,l2,frac,output,datafile,batchfrac)
addpath(genpath('../../single_W/'));
hiddenSize = 25;
params.etat=.05;
params.etaw=.5;
params.lambda_t=l1;
params.lambda_w=l2;
params.margin = 1;

load('../data/skipwiki25.mat');
load('../data/theta_init_25.mat');
load(datafile);

train_data = [train_data test_data valid_data];

%use adagrad
params.epochs = 5;
params.data= train_data(1:round(frac*length(train_data)));
fprintf('Training on %i data using %f and %f\n',length(params.data),l1,l2);

params.batchsize = round(batchfrac*length(params.data));
fprintf('Training on %d instances.\n',length(params.data));

[adatheta, cost] = AGWords(theta, params, hiddenSize, words, We_orig, output);
end