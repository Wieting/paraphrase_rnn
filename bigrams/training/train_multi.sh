#$1 lam 1
#$2 lam 2
#$3 fraction of training data to use
#$4 output

matlab -nodisplay -nodesktop -nojvm -nosplash -r "train_multi($1,$2,$3,'$4');quit" &