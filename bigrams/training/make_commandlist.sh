python make_tti_commands_single.py ../../make_data/bigrams/adj-noun-base-xl.txt.mat adj-noun-xl 0.05 > adj-noun_test.txt
python make_tti_commands_single.py ../../make_data/bigrams/verb-noun-base-xl.txt.mat verb-noun-xl 0.05 > verb-noun_test.txt
python make_tti_commands_single.py ../../make_data/bigrams/noun-noun-base-xl.txt.mat noun-noun-xl 0.05 > noun-noun_test.txt

cat adj-noun_test.txt verb-noun_test.txt noun-noun_test.txt > all_test.txt

python make_tti_commands_single.py ../../make_data/bigrams/adj-noun-base-xl.txt.mat adj-noun-xl 1.0 > adj-noun.txt
python make_tti_commands_single.py ../../make_data/bigrams/verb-noun-base-xl.txt.mat verb-noun-xl 1.0 > verb-noun.txt
python make_tti_commands_single.py ../../make_data/bigrams/noun-noun-base-xl.txt.mat noun-noun-xl 1.0 > noun-noun.txt

cat adj-noun.txt verb-noun.txt noun-noun.txt > all.txt